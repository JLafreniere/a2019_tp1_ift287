package HumanBody;

import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Connection extends BodyPart
{
    private int id;
    private ArrayList<Integer> to;
    
    public Connection() {
    	super("Connection");
        to = new ArrayList<Integer>();
    }
    
    public Connection(JsonObject val){
        super("Connection");
        
        this.id = val.getInt("id");
        this.to = new ArrayList<Integer>();
        JsonArray tos = val.getJsonArray("to");

        for (javax.json.JsonValue jsonValue : tos) {
        	to.add(Integer.parseInt(jsonValue.toString()));
		}
        
    }

    
    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public ArrayList<Integer> getTo()
    {
        return to;
    }
    public void setTo(ArrayList<Integer> to)
    {
        this.to = to;
    }
    
    public  JsonObjectBuilder toJson() {
    	
    	JsonObjectBuilder builder = Json.createObjectBuilder();
 	    builder.add("id", this.getId());
 	    
	    JsonArrayBuilder cTo = Json.createArrayBuilder();
	    for( int c_id : this.to)
	        cTo.add(c_id);
	    
	    builder.add("to", cTo);
	    
	    return builder;
    }

	@Override
	public Element toXML(Document document) {
        Element element = document.createElement(this.tagName);
        element.setAttribute("id", String.valueOf(this.id));
        
        for (int to: this.to)
        {
            Element toNode = document.createElement("to");
            toNode.setAttribute("id", String.valueOf(to));
            element.appendChild(toNode);
        }
        
        return element;
	}
}
