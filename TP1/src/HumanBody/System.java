package HumanBody;

import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class System extends BodyPart
{
    private ArrayList<Flow> flows;
    private String name;
    private int id;
    private int type;
    
    public System() {
    	super("System");
        flows = new ArrayList<Flow>();     
    }
    
    public System(JsonObject obj) {
        super("System");
        this.name = obj.getString("name");
        this.id = obj.getInt("id");
        this.type = obj.getInt("type");
        this.flows = new ArrayList<Flow>();
        
        JsonArray flows = obj.getJsonArray("Flows");
        
        for(JsonValue jsonFlow : flows){
            JsonObject fl = (JsonObject)jsonFlow;
            this.flows.add(new Flow(fl));
        }

    }

    public ArrayList<Flow> getFlows()
    {
        return flows;
    }

    public void setFlows(ArrayList<Flow> flows)
    {
        this.flows = flows;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }
    
    public  JsonObjectBuilder toJson() {
	    JsonObjectBuilder builder = Json.createObjectBuilder();
	    builder.add("name", this.getName());
	    builder.add("bodyID", this.getId());
	    builder.add("type", this.getType());
	    builder.add("id", this.getId());
 	    
	    JsonArrayBuilder sFlows = Json.createArrayBuilder();
	    for( Flow f : this.flows)
	        sFlows.add(f.toJson());
	    
	    
	    builder.add("Flows", sFlows);
	
	    return builder;
    }

    @Override
    public Element toXML(Document document) {
        Element element = document.createElement(this.tagName);
        element.setAttribute("name", this.name);
        element.setAttribute("id", String.valueOf(this.id));
        element.setAttribute("type", String.valueOf(this.type));
        
        for (Flow flow : this.flows)
        {
            element.appendChild(flow.toXML(document));
        }
        return element;
    }

    
    
   
}
