package HumanBody;
import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class HumanParser extends DefaultHandler {
    
    public MainBody mainBody;
    public HumanBody.System currentSystem;
    public Flow currentFlow;
    public HumanBody.Connection currentConnection;
    public boolean isReadingConnectible = false;
    public ArrayList<Connectible> currentConnectibles;
    
    public MainBody getMainBody() {
    	return mainBody;
    }
    
    public void startDocument() throws SAXException
    {
        java.lang.System.out.println("start document   : ");
    }
    
    public void endDocument() throws SAXException
    {
        MainBody b = mainBody;
        java.lang.System.out.println("end document");  
    }
    
    public void startElement(String uri, String localName, String qName,  Attributes attributes) throws SAXException {
        if(isReadingConnectible) {
            Connectible c = new Connectible(qName);
            c.setId(Integer.parseInt(attributes.getValue("id")));
            c.setName(attributes.getValue("name"));
            
            if(attributes.getValue("endRadius") != null) {                
                c.setEndRadius(Double.parseDouble(attributes.getValue("endRadius")));
            }
            if(attributes.getValue("volume") != null) {                
                c.setVolume(Double.parseDouble(attributes.getValue("volume")));
            }
            if(attributes.getValue("startRadius") != null) {                
                c.setStartRadius(Double.parseDouble(attributes.getValue("startRadius")));
            }
            if(attributes.getValue("length") != null) {                
                c.setLength(Double.parseDouble(attributes.getValue("length")));
            }
            currentConnectibles.add(c);
        }
        if(qName == "MainBody") {
            mainBody = new MainBody();
            mainBody.setBodyID(Integer.parseInt(attributes.getValue("bodyID")));
            mainBody.setBodyName(attributes.getValue("bodyName"));
            
        } 
        else if(qName == "System") {
            HumanBody.System s = new HumanBody.System();
            s.setId(Integer.parseInt(attributes.getValue("id")));
            s.setName(attributes.getValue("name"));
            s.setType(Integer.parseInt(attributes.getValue("type")));
            this.currentSystem = s;
            

            mainBody.getSystems().add(s);
        } 
        else if(qName == "Flow") {
            Flow f = new Flow();
            f.setId(Integer.parseInt(attributes.getValue("id")));
            f.setName(attributes.getValue("name"));
            currentSystem.getFlows().add(f);
            currentFlow = f;
        }
        else if(qName == "Connection") {
            HumanBody.Connection c = new HumanBody.Connection();
            c.setId(Integer.parseInt(attributes.getValue("id")));
            currentFlow.getConnections().add(c);
            currentConnection = c;
        }        
        else if(qName == "to") {
            int to = Integer.parseInt(attributes.getValue("id"));
            currentConnection.getTo().add(to);
        }
        else if(qName == "Organ") {
            Organ o = new Organ();
            o.setId(Integer.parseInt(attributes.getValue("id")));
            o.setName(attributes.getValue("name"));
            o.setSystemID(Integer.parseInt(attributes.getValue("systemID")));
            mainBody.getOrgans().add(o);
        }
        else if(qName == "Connectible") {
            isReadingConnectible = true;
            currentConnectibles = new ArrayList<Connectible>();
        }
    }
    
    public void endElement(java.lang.String uri, java.lang.String localName, java.lang.String qName) {
        if(qName == "Connectible") {
            isReadingConnectible = false;
            currentFlow.setConnectibles(currentConnectibles);
        }   
    }
    
}