package HumanBody;

import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Flow extends BodyPart
{
    private ArrayList<Connectible> connectibles;
    private ArrayList<HumanBody.Connection> connections;
    public int getId()
    {
        return id;
    }
    
    public Flow(JsonObject obj)
    {
        super("Flow");
        this.name = obj.getString("name");
        this.id = obj.getInt("id");
        this.connectibles = new ArrayList<Connectible>();
        this.connections = new ArrayList<Connection>();
        
        JsonArray connectibles = obj.getJsonArray("Connectibles");
        JsonArray connections = obj.getJsonArray("Connections");
        
        for(JsonValue jsonConnectible : connectibles){
            JsonObject val = (JsonObject)jsonConnectible;
            this.connectibles.add(new Connectible(val));
        }
       
        for(JsonValue jsonConnection : connections){
            JsonObject val = (JsonObject)jsonConnection;
            this.connections.add(new Connection(val));
        }
        
    }


    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    private int id;
    private String name;
    
    public Flow() {
    	super("Flow");
        connectibles = new ArrayList<Connectible>();
        connections = new ArrayList<HumanBody.Connection>();
    }

    public ArrayList<Connectible> getConnectibles()
    {
        return connectibles;
    }

    public void setConnectibles(ArrayList<Connectible> connectibles)
    {
        this.connectibles = connectibles;
    }

    public ArrayList<HumanBody.Connection> getConnections()
    {
        return connections;
    }

    public void setConnections(ArrayList<HumanBody.Connection> connections)
    {
        this.connections = connections;
    }
    
    public  JsonObjectBuilder toJson() {
    	
    	JsonObjectBuilder builder = Json.createObjectBuilder();
    	builder.add("name", this.getName());
    	builder.add("id", this.getId());
	    
	    JsonArrayBuilder fConnections = Json.createArrayBuilder();
	    for( Connection c : this.connections)
	        fConnections.add(c.toJson());
	    
	    JsonArrayBuilder fConnectibles = Json.createArrayBuilder();
	    for( Connectible c : this.connectibles)
	        fConnectibles.add(c.toJson());
	    
	    builder.add("Connectibles", fConnectibles);
	    builder.add("Connections", fConnections);
	    return builder;
    }

	@Override
	public Element toXML(Document document) {
        Element element = document.createElement(this.tagName);
        element.setAttribute("id", String.valueOf(this.id));
        element.setAttribute("name", this.name);
        
        Element connectiblesNode = document.createElement("Connectibles");
        for (Connectible connectible : this.connectibles)
        {
            connectiblesNode.appendChild(connectible.toXML(document));
        }
        element.appendChild(connectiblesNode);
        
        Element connectionsNode = document.createElement("Connections");
        for (Connection connection: this.connections)
        {
            connectionsNode.appendChild(connection.toXML(document));
        }
        element.appendChild(connectionsNode);
        
        return element;
	}
}
