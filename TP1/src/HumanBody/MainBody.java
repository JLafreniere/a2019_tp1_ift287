package HumanBody;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import javax.json.*;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;

import javax.xml.transform.TransformerFactory;

import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class MainBody
{
    private String bodyName;
    private int bodyID;
    private ArrayList<HumanBody.System> systems;
    private ArrayList<Organ> organs;
    
    public MainBody() {
        systems = new ArrayList<System>();
        organs = new ArrayList<Organ>();
    }
    
    public MainBody(JsonObject mainBodyObject) {
    	  this.bodyName = mainBodyObject.getString("bodyName");
          this.bodyID = mainBodyObject.getInt("bodyID");
          
          this.systems = new ArrayList<System>();
          this.organs = new ArrayList<Organ>();
          
          JsonArray systems = mainBodyObject.getJsonArray("Systems");
          
          for(JsonValue jsonSystem : systems){
              JsonObject obj = (JsonObject)jsonSystem;
              this.systems.add(new HumanBody.System(obj));
          }
          
          
          JsonArray organs = mainBodyObject.getJsonArray("Organs");
          
          for(JsonValue jsonOrgan : organs){
              JsonObject obj = (JsonObject)jsonOrgan;
              this.organs.add(new Organ(obj));
          }

    }

    public String getBodyName()
    {
        return bodyName;
    }

    public void setBodyName(String bodyName)
    {
        this.bodyName = bodyName;
    }

    public int getBodyID()
    {
        return bodyID;
    }

    public void setBodyID(int bodyID)
    {
        this.bodyID = bodyID;
    }

    public ArrayList<HumanBody.System> getSystems()
    {
        return systems;
    }

    public void setSystems(ArrayList<HumanBody.System> systems)
    {
        this.systems = systems;
    }

    public ArrayList<Organ> getOrgans()
    {
        return organs;
    }

    public void setOrgans(ArrayList<Organ> _organs)
    {
        organs = _organs;
    }
    
    public  JsonObjectBuilder toJson() {
	    JsonObjectBuilder builder = Json.createObjectBuilder();
	    builder.add("bodyName", this.getBodyName());
	    builder.add("bodyID", this.bodyID);
	    
	    JsonArrayBuilder bSystems = Json.createArrayBuilder();
	    for( System s : this.systems)
	        bSystems.add(s.toJson());
	    
	    JsonArrayBuilder bOrgans = Json.createArrayBuilder();
	    for( Organ o : this.organs)
	        bOrgans.add(o.toJson());
	    
	    builder.add("Systems", bSystems);
	    builder.add("Organs", bOrgans);
	    return builder;
    }

    protected Document buildDom() throws ParserConfigurationException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        factory.setIgnoringElementContentWhitespace(true);
        factory.setIgnoringComments(true);

        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();

        Element mainBodyNode = document.createElement("MainBody");
        mainBodyNode.setAttribute("bodyName", this.bodyName);
        mainBodyNode.setAttribute("bodyID", String.valueOf(this.bodyID));

        Element systemsNode = document.createElement("Systems");
        for (HumanBody.System system : this.systems)
        {
            systemsNode.appendChild(system.toXML(document));
        }
        mainBodyNode.appendChild(systemsNode);
        
        Element organsNode = document.createElement("Organs");
        for (Organ organ: this.organs)
        {
            organsNode.appendChild(organ.toXML(document));
        }
        mainBodyNode.appendChild(organsNode);
        
        document.appendChild(mainBodyNode);
        return document;
    }

    protected void writeXMLFile(String fileName, Document document) throws FileNotFoundException,
            TransformerFactoryConfigurationError, TransformerConfigurationException, TransformerException
    {
        FileOutputStream output = new FileOutputStream(fileName);
        PrintStream out = new PrintStream(output);
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", Integer.toString(2));

        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(out);
        transformer.transform(source, result);
    }
    
    public void toXML(String fileName)
    {
        Document document;
        try
        {
            document = buildDom();
            writeXMLFile(fileName, document);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }



}
