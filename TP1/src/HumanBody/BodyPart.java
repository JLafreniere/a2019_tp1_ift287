package HumanBody;

import javax.json.JsonObjectBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public abstract class BodyPart
{
    public BodyPart(String tagName)
    {
        this.tagName = tagName;
    }
    
    protected String tagName;
    public abstract Element toXML(Document document);
    public abstract JsonObjectBuilder toJson();

}
