package HumanBody;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Organ extends BodyPart
{
    private int id;
    private int systemID;
    private String name;
    
    public Organ(JsonObject val)
    {
        super("Organ");
        this.name = val.getString("name");
        this.id = val.getInt("id");
        this.systemID = val.getInt("systemID");

    }

    
    public Organ () {
    	super("Organ");
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getSystemID()
    {
        return systemID;
    }

    public void setSystemID(int systemID)
    {
        this.systemID = systemID;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    };
    
    public  JsonObjectBuilder toJson() {
	    JsonObjectBuilder builder = Json.createObjectBuilder();
	    builder.add("id", this.getId());
	    builder.add("systemID", this.getSystemID());
	    builder.add("name", this.getName());
	   
	    return builder;
    }


	@Override
	public Element toXML(Document document) {
        Element element = document.createElement(this.tagName);
        element.setAttribute("name", this.name);
        element.setAttribute("id", String.valueOf(this.id));
        element.setAttribute("systemID", String.valueOf(this.systemID));
        
        return element;

	}
    
}
