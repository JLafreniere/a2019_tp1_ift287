package HumanBody;

import java.math.BigDecimal;

import javax.json.Json;

import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Connectible extends BodyPart {
	private String name;
	private int id;
	private double startRadius;
	private double endRadius;
	private double volume;
	private double length;

	public Connectible(String connectibleType) {
		super(connectibleType);
	}

	public Connectible(JsonObject val) {
		super(val.getString("tagName"));

		this.name = val.getString("name");
		this.id = val.getInt("id");
		
		if (val.containsKey("volume")) {
			this.volume = BigDecimal.valueOf(val.getJsonNumber("volume").doubleValue()).floatValue();
		}

		if (val.containsKey("startRadius")) {
			this.startRadius = BigDecimal.valueOf(val.getJsonNumber("startRadius").doubleValue()).floatValue();
		}

		if (val.containsKey("endRadius")) {
			this.endRadius = BigDecimal.valueOf(val.getJsonNumber("endRadius").doubleValue()).floatValue();
		}

		if (val.containsKey("length")) {
			this.length = BigDecimal.valueOf(val.getJsonNumber("length").doubleValue()).floatValue();
		}

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getStartRadius() {
		return startRadius;
	}

	public void setStartRadius(double startRadius) {
		this.startRadius = startRadius;
	}

	public double getEndRadius() {
		return endRadius;
	}

	public void setEndRadius(double endRadius) {
		this.endRadius = endRadius;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public JsonObjectBuilder toJson() {
		JsonObjectBuilder builder = Json.createObjectBuilder();

		builder.add("tagName", this.tagName);
		
		if (this.getStartRadius() > 0.0) {
			builder.add("startRadius", this.getStartRadius());
		}

		if (this.getEndRadius() > 0.0) {
			builder.add("endRadius", this.getEndRadius());
		}

		if (this.getVolume() > 0.0) {
			builder.add("volume", this.getVolume());
		}

		if (this.getLength() > 0.0) {
			builder.add("length", this.getLength());
		}

		builder.add("name", this.getName());
		builder.add("id", this.getId());

		return builder;
	}

	@Override
	public Element toXML(Document document) {
		Element element = document.createElement(this.tagName);
		element.setAttribute("name", this.name);
		element.setAttribute("id", String.valueOf(this.id));

		if (this.volume != 0.0) {
			element.setAttribute("volume", String.valueOf(this.volume));
		}

		if (this.startRadius != 0.0) {
			element.setAttribute("startRadius", String.valueOf(this.startRadius));
		}

		if (this.endRadius != 0.0) {
			element.setAttribute("endRadius", String.valueOf(this.endRadius));
		}

		if (this.length != 0.0) {
			element.setAttribute("length", String.valueOf(this.length));
		}

		return element;

	}

}
